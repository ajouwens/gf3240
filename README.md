GF3240 - Garmin fenix 3 - 240 degrees watchface
===============================================

Description
-----------
This is a watch face inspired on a Moto screen mock(?). It shows the time on a 240 degrees scale instead of the common 360 degrees. For convenience there's also a digital time.

Version history
---------------
###V1.3
+ the performance of the watch should be ok now, 
+ added a background image for the static parts of the watchface.
+ added the arc for battery percentage with a red triangle indicator.
+ added the arc for daily step progress percentage with a blue triangle indicator. 

###V1.2
+ removed the battery & step progress indicators. They made the watch respond sluggish.

###V1.1:
+ bug fix for the 12 hour clock.
+ added small "arc" indicators for:
+ battery (dark grey or red ( when less than 25% left))
+ daily steps progress (white, or omitted when there's no step goal set )

###V1.0:
+ 12 hour clock has AM/PM indicators
+ 24 hour clock changes clock marks from 0-12 to 12-24
+ second hand omitted, because there's no use for it in low power mode
+ shows day, day of the week and month

Garmin forum
------------
https://forums.garmin.com/showthread.php?249763-Watchface-GF3240-Garmin-Fenix3-240-degrees

Watch Permissions
-----------------
###This app requires access to:
+ Steps data (number of daily steps you take)
+ Your Garmin Connect™ fitness profile