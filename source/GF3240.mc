using Toybox.Graphics as Gfx;
using Toybox.System as Sys;
using Toybox.Lang as Lang;
using Toybox.Math as Math;
using Toybox.Time as Time;
using Toybox.Time.Gregorian as Calendar;
using Toybox.WatchUi as Ui;
using Toybox.Application as App;
using Toybox.ActivityMonitor as Act;

class GF3240 extends Ui.WatchFace {
    var pi9 = Math.PI / 9.0;
    var pi4 = Math.PI / 4.5;
    var pi2 = Math.PI / 2.0;
    var piP = 2.4 * Math.PI / 180;
    var cx;
    var cy;
    var width;
    var height;
    var is24Hour = false;
    var bckgrnd;

    //! Constructor
    function initialize() {
        is24Hour = Sys.getDeviceSettings().is24Hour;
        bckgrnd = Ui.loadResource(Rez.Drawables.bckgrnd);
    }

    //! Load resources
    function onLayout(dc) {
        width = dc.getWidth();
        height = dc.getHeight();
        cx = width / 2;
        cy = height / 2;
    }

    function onShow() {}

    function onHide() {}

    //! Handle the update event
    function onUpdate(dc) {
        dc.drawBitmap(0, 0, bckgrnd);

        var hour= Sys.getClockTime().hour;
        var min = Sys.getClockTime().min;
        var stepGoal = (Act.getInfo().stepGoal).toDouble();
        var steps = (Act.getInfo().steps).toDouble();
        var battPerc = (Sys.getSystemStats().battery).toDouble();

        drawDateTime(dc, hour, min);
        drawHourMarks(dc, hour);
        drawMinuteMarks(dc);

        // Draw the battery indicator
        dc.setColor(Gfx.COLOR_RED, Gfx.COLOR_TRANSPARENT);
        battPerc = (battPerc * piP) - (pi2);
        drawMarker(dc, battPerc, 32);

        // Draw the step indicator
        if (stepGoal != 0) {
            var relPos = steps <=  stepGoal ? (steps / stepGoal) * 100.0 : 100.0;
            dc.setColor(Gfx.COLOR_BLUE, Gfx.COLOR_TRANSPARENT);
            relPos = (relPos * piP) - (pi2);
            drawMarker(dc, relPos, 18);
        }

        // Draw the hour hand
        dc.setColor(Gfx.COLOR_BLUE, Gfx.COLOR_TRANSPARENT);
        hour = (hour % 12) + (min / 60.0);
        hour = (hour * pi9) - (pi2);
        drawHand(dc, hour, 94, 7);

        // Draw the minute hand
        dc.setColor(Gfx.COLOR_BLUE, Gfx.COLOR_TRANSPARENT);
        min = (min / 10.0);
        min = (min * pi4) - (pi2);
        drawHand(dc, min, 54, 7);
    }

    function drawHourMarks(dc, hour) {
        var hourFont = Gfx.FONT_TINY;
        if (is24Hour && hour >= 12) {
            dc.drawText(cx-78,cy   , hourFont, "12", Gfx.TEXT_JUSTIFY_VCENTER);
            dc.drawText(cx-72,cy-31, hourFont, "13", Gfx.TEXT_JUSTIFY_VCENTER);
            dc.drawText(cx-58,cy-55, hourFont, "14", Gfx.TEXT_JUSTIFY_VCENTER);
            dc.drawText(cx-37,cy-77, hourFont, "15", Gfx.TEXT_JUSTIFY_VCENTER);
            dc.drawText(cx-9 ,cy-88, hourFont, "16", Gfx.TEXT_JUSTIFY_VCENTER);
            dc.drawText(cx+23,cy-88, hourFont, "17", Gfx.TEXT_JUSTIFY_VCENTER);
            dc.drawText(cx+51,cy-75, hourFont, "18", Gfx.TEXT_JUSTIFY_VCENTER);
            dc.drawText(cx+73,cy-55, hourFont, "19", Gfx.TEXT_JUSTIFY_VCENTER);
            dc.drawText(cx+88,cy-31, hourFont, "20", Gfx.TEXT_JUSTIFY_VCENTER);
            dc.drawText(cx+95,cy   , hourFont, "21", Gfx.TEXT_JUSTIFY_VCENTER);
            dc.drawText(cx+88,cy+31, hourFont, "22", Gfx.TEXT_JUSTIFY_VCENTER);
            dc.drawText(cx+73,cy+55, hourFont, "23", Gfx.TEXT_JUSTIFY_VCENTER);
            dc.drawText(cx+48,cy+75, hourFont, "24", Gfx.TEXT_JUSTIFY_VCENTER);
        } else {
            dc.drawText(cx-85,cy   , hourFont,  "0", Gfx.TEXT_JUSTIFY_VCENTER);
            dc.drawText(cx-80,cy-31, hourFont,  "1", Gfx.TEXT_JUSTIFY_VCENTER);
            dc.drawText(cx-65,cy-55, hourFont,  "2", Gfx.TEXT_JUSTIFY_VCENTER);
            dc.drawText(cx-40,cy-77, hourFont,  "3", Gfx.TEXT_JUSTIFY_VCENTER);
            dc.drawText(cx-13,cy-88, hourFont,  "4", Gfx.TEXT_JUSTIFY_VCENTER);
            dc.drawText(cx+20,cy-88, hourFont,  "5", Gfx.TEXT_JUSTIFY_VCENTER);
            dc.drawText(cx+48,cy-77, hourFont,  "6", Gfx.TEXT_JUSTIFY_VCENTER);
            dc.drawText(cx+73,cy-55, hourFont,  "7", Gfx.TEXT_JUSTIFY_VCENTER);
            dc.drawText(cx+88,cy-31, hourFont,  "8", Gfx.TEXT_JUSTIFY_VCENTER);
            dc.drawText(cx+92,cy   , hourFont,  "9", Gfx.TEXT_JUSTIFY_VCENTER);
            dc.drawText(cx+88,cy+31, hourFont, "10", Gfx.TEXT_JUSTIFY_VCENTER);
            dc.drawText(cx+73,cy+55, hourFont, "11", Gfx.TEXT_JUSTIFY_VCENTER);
            dc.drawText(cx+48,cy+75, hourFont, "12", Gfx.TEXT_JUSTIFY_VCENTER);
        }
    }

    function drawMinuteMarks(dc) {
        var minuteFont = Gfx.FONT_XTINY;
        dc.drawText(cx-48,cy   , minuteFont, "0" , Gfx.TEXT_JUSTIFY_VCENTER);
        dc.drawText(cx-32,cy-32, minuteFont, "10", Gfx.TEXT_JUSTIFY_VCENTER);
        dc.drawText(cx-3 ,cy-50, minuteFont, "20", Gfx.TEXT_JUSTIFY_VCENTER);
        dc.drawText(cx+32,cy-43, minuteFont, "30", Gfx.TEXT_JUSTIFY_VCENTER);
        dc.drawText(cx+52,cy-20, minuteFont, "40", Gfx.TEXT_JUSTIFY_VCENTER);
        dc.drawText(cx+52,cy+15, minuteFont, "50", Gfx.TEXT_JUSTIFY_VCENTER);
        dc.drawText(cx+30,cy+40, minuteFont, "60", Gfx.TEXT_JUSTIFY_VCENTER);
    }

    //! Draw the watch hand
    //! @param dc Device Context to Draw
    //! @param angle Angle to draw the watch hand
    //! @param length Length of the watch hand
    //! @param width Width of the watch hand
    function drawHand(dc, angle, length, width) {
        var coords = [ [-(width/2),0], [0, -length], [width/2, 0] ];
        var result = new [coords.size()];
        var cos = Math.cos(angle);
        var sin = Math.sin(angle);

        // Transform the coordinates
        for (var i = 0; i < coords.size(); i += 1) {
            var x = (coords[i][0] * cos) - (coords[i][1] * sin) + cx;
            var y = (coords[i][0] * sin) + (coords[i][1] * cos) + cy;
            result[i] = [x, y];
        }
        dc.fillPolygon(result);
    }

    function drawMarker(dc, angle, position) {
        var coords = [ [-(5),-position+7], [0, -position], [5, -position+7] ];
        var result = new [coords.size()];
        var cos = Math.cos(angle);
        var sin = Math.sin(angle);

        // Transform the coordinates
        for (var i = 0; i < coords.size(); i += 1) {
            var x = (coords[i][0] * cos) - (coords[i][1] * sin) + cx;
            var y = (coords[i][0] * sin) + (coords[i][1] * cos) + cy;
            result[i] = [x, y];
        }
        dc.fillPolygon(result);
    }

    function drawDateTime(dc, hour, min) {
        var now = Time.now();
        var info = Calendar.info(now, Time.FORMAT_LONG);
        var dHour = hour;

        var dateStr;
        if (is24Hour) {
            dateStr = Lang.format("$1$ $2$ $3$", [info.day_of_week, info.day, info.month]);
        } else {
            dHour = hour > 12 ? hour % 12 : hour;
            dateStr = Lang.format("$1$ $2$ $3$", [info.day_of_week, info.month, info.day]);
        }
        var timeStr = Lang.format("$1$:$2$", [dHour, min.format("%02d")]);

        dc.setColor(Gfx.COLOR_RED, Gfx.COLOR_TRANSPARENT);
        dc.drawText(cx-85, cy+26, Gfx.FONT_TINY, dateStr, Gfx.TEXT_JUSTIFY_LEFT);

        dc.setColor(Gfx.COLOR_WHITE, Gfx.COLOR_TRANSPARENT);
        dc.drawText(cx-58, cy+35, Gfx.FONT_NUMBER_MEDIUM , timeStr, Gfx.TEXT_JUSTIFY_LEFT);

        if (!is24Hour) {
            dc.drawText(cx-85, cy+43, Gfx.FONT_TINY, getHourUI(hour, is24Hour)[1] , Gfx.TEXT_JUSTIFY_LEFT);
        }
    }

    function getHourUI(hour, is24Hour) {
        if (!is24Hour) {
            if(hour == 0) {
                return [12, "AM"];
            } else if(hour == 12) {
                return [12, "PM"];
            } else if(hour < 12){
                return [hour.toNumber(), "AM"];
            } else {
                return [(hour.toNumber() % 12), "PM"];
            }
        }
        return [hour.toNumber(), ""];
    }

}

class GF3240Watch extends App.AppBase {
    function onStart() {}

    function onStop() {}

    function getInitialView() {
        return [new GF3240()];
    }
}
